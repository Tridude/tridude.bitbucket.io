
Tabulator.prototype.extendModule("format", "formatters", {
    datetimeheat:function(cell, formatterParams){
        var inputFormat = formatterParams.inputFormat || "YYYY-MM-DD hh:mm:ss";
        var	outputFormat = formatterParams.outputFormat || "DD/MM/YYYY hh:mm:ss";
        var value = cell.getValue();
        var newDatetime = moment(value, inputFormat);
        if (newDatetime.isValid()) {
            dateDiff = moment().diff(newDatetime, "days");
            if (dateDiff == 0) {
                cell.getElement().classList.add("new_24h")
                // cell.getRow().getElement().style.backgroundColor = "#239B56";
            } else if (dateDiff == 1) {
                cell.getElement().classList.add("new_48h")
                // cell.getRow().getElement().style.backgroundColor = "#28B463";
            } else if (dateDiff == 2) {
                cell.getElement().classList.add("new_72h")
                // cell.getRow().getElement().style.backgroundColor = "#2ECC71";
            } else if (dateDiff == 3) {
                cell.getElement().classList.add("new_96h")
                // cell.getRow().getElement().style.backgroundColor = "#58D68D";
            }
            return newDatetime.format(outputFormat);
        } else {
            return value;
        }
    },

    imagetitle:function(cell, formatterParams) {
        // create text element
        var text = document.createTextNode(cell.getData().name);
        // create image element
        // todo - replace with own functions
        var img = Tabulator.prototype.moduleBindings.format.prototype.formatters.image(cell, formatterParams);
        // create link element
        var link = document.createElement("a");
        var url = "";
        if (formatterParams.urlField) {
            url = cell.getData()[formatterParams.urlField];
        }
        link.setAttribute("href", url)
        if (formatterParams.target) {
            link.setAttribute("target", formatterParams.target);
        }
        // format
        link.appendChild(img);
        link.appendChild(document.createElement("br"));
        link.appendChild(text);
        return link;
    },

    price:function(cell, formatterParams) {
        var price = cell.getValue();
        var currency = cell.getData()["currency"];
        if (currency == "JPY") {
            price = price + "円"
        } else if (currency == "USD") {
            price = "$" + price
        } else {
            price = price + " " + currency
        }
        return price
    },
});

Tabulator.prototype.extendModule("sort", "sorters", {
    imagetitle:function(a, b, aRow, bRow) {
        return String(aRow.getData()["name"]).toLocaleLowerCase().localeCompare(String(bRow.getData()["name"]).toLocaleLowerCase());
    },

    price:function(a, b, aRow, bRow) {
        // convert prices to USD first
        // rough estimates
        function fx(v, c) {
            if (c == "JPY") {
                return v * 0.009
            }
            return v
        };
        av = fx(parseFloat(String(a)), aRow.getData()["currency"]);
        bv = fx(parseFloat(String(b)), bRow.getData()["currency"]);
        return av - bv;
    }
});



window.onload = function() {
    var table = new Tabulator("#sale-table", {
        locale:true,
        langs: {
            "ja": {
                "columns": {
                    "banner_url": "タイトル",
                    "sale_price": "セール価格",
                    "currency": "通貨",
                    "percent": "割引率",
                    "start": "開始日",
                    "end": "終了日",
                    "store_name": "サイト",
                }
            }
        },
        height: '100%',
        layout:"fitColumns",
        columns: [
            {title:"Title", field:"banner_url", formatter:"imagetitle", widthGrow:5,
                formatterParams: {
                    height: 100,
                    labelField: "name",
                    urlField: "link",
                    target: "_blank",
                },
                sorter:"imagetitle",
            },
            {field:"name", visible:false},
            {title:"Price", field:"sale_price", formatter:"price", sorter:"price"},
            {title:"Currency", field:"currency", visible:false},
            {title:"% Off", field:"percent"},
            {title:"Start Date", field:"start", widthGrow:2, visible:true,
                formatter:"datetimeheat",
                formatterParams: {
                    outputFormat:"L LT",
                },
            },
            {title:"End Date", field:"end", widthGrow:2,
                formatter:"datetime",
                formatterParams: {
                    outputFormat:"L LT",
                },
            },
            {title:"Store", field:"store_name", headerFilter:"input"},
            {field:"link", visible: false},
        ],
        initialSort: [
            {column:"name", dir:"asc"},
            {column:"start", dir:"desc"},
        ],
    });
    $.getJSON("sales.json", function(data){
        table.setData(data);
    });
    var url = new URL(window.location.href)
    table.setHeaderFilterValue("store_name", url.searchParams.get("store"));
};
